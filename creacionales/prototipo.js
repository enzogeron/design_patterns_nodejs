const perro = {
    raza: 'Kilterrier',
    ladrar: function () {
        console.log(`Gauu! ${this.raza}`)
    }
}

const kiltro = Object.create(perro)

kiltro.ladrar()

console.log(kiltro)

kiltro.raza = 'Supersayayin'

kiltro.ladrar()