const Emitter = (() => {
    const topics = {}
    const hOP = topics.hasOwnProperty

    return {
        on: (topic, listener) => {
            if (!hOP.call(topics, topic)) topics[topic] = []
            topics[topic].push(listener)
        },
        emit: (topic, info) => {
            if (!hOP.call(topics, topic)) return
            topics[topic].forEach(item =>
                item(info != undefined ? info : {}))
        }
    }
})()

Emitter.on('evento01', x => console.log(`Evento01: ${x}`))

Emitter.emit('evento01', 'call desde emit')

// En nodejs

const EmitterI = require('events')

const emitter = new EmitterI()

emitter.on('evento02', x => console.log(`Evento02: ${x}`))

emitter.emit('evento02', 'call desde emit')